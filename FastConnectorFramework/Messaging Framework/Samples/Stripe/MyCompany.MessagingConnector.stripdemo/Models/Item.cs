﻿namespace MyCompany.MessagingConnector.stripdemo.Models
{

    public class Rootobject
    {
        public int created { get; set; }
        public bool livemode { get; set; }
        public string id { get; set; }
        public string type { get; set; }
        public string _object { get; set; }
        public object request { get; set; }
        public int pending_webhooks { get; set; }
        public string api_version { get; set; }
        public Data data { get; set; }
    }

    public class Data
    {
        public Customer _object { get; set; }
    }

    public class Customer
    {
        public string id { get; set; }
        public string _object { get; set; }
        public int account_balance { get; set; }
        public int created { get; set; }
        public string currency { get; set; }
        public object default_source { get; set; }
        public bool delinquent { get; set; }
        public string description { get; set; }
        public object discount { get; set; }
        public string email { get; set; }
        public bool livemode { get; set; }
        public Metadata metadata { get; set; }
        public object shipping { get; set; }
        public Sources sources { get; set; }
        public Subscriptions subscriptions { get; set; }
    }

    public class Metadata
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postal { get; set; }
    }

    public class Sources
    {
        public string _object { get; set; }
        public object[] data { get; set; }
        public bool has_more { get; set; }
        public int total_count { get; set; }
        public string url { get; set; }
    }

    public class Subscriptions
    {
        public string _object { get; set; }
        public object[] data { get; set; }
        public bool has_more { get; set; }
        public int total_count { get; set; }
        public string url { get; set; }
    }

}
