﻿namespace Sample.Connector.Weather
{
    using Simple.Connector.Framework;
    using Simple.Connector.Framework.Http;

    [SimpleConnector(
        ConnectorSettings.ConnectorTypeId,
        ConnectorSettings.Name,
        ConnectorSettings.Description,
        typeof(WeatherConnector),
        ConnectorSettings.SupportsCloud,
        ConnectorSettings.ConnectorVersion)]
    public class WeatherConnector : HttpConnectorBase<WeatherConnectionInfo>
    {
        public WeatherConnector()
            : base(ConnectorSettings.ConnectorTypeId, "Sample", "http://www.scribesoft.com", "b4acccbf-babc-4770-a3e5-62fdadb130a5")
        {
        }
    }
}
