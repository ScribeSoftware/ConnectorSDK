﻿namespace Sample.Connector.Weather
{
    using System;
    using System.Collections.Generic;
    using Simple.Connector.Framework.Http;
    using Models;
    using Simple.Connector.Framework;

    public class WeatherConnectionInfo : XmlHttpConnectionInfoBase<WeatherConnectionInfo>
    {
        protected override ISerializer Serializer { get { return new WeatherSerializer(); } }
        public string BaseUrl { get; set; }

        protected override Func<WeatherConnectionInfo, IConnectionConfiguration> ConnectionConfiguration()
        {
            return this.Connection.Configure("GET", connInfo => connInfo.BaseUrl + "/ndfdBrowserClientByDay.php")
                .ConnectionInfoToBaseUrl(info => info.BaseUrl) // Set the BaseUrl for subsequent calls
                .ToQuery("zipCodeList", "03104")
                .ToQuery("format", "24+hourly")
                .ToQuery("numDays", "7")
                .End(); // Finish the configuration
        }
        protected override IList<HttpQueryRegistration> ConfigureQueries()
        {
            var weather = this.Queries.EnumerateResponseAs<Weather, WeatherResponse>("/xml/sample_products/browser_interface/ndfdBrowserClientByDay.php", r => r.Weathers)
                .ToQuery("zipCodeList", "03104")
                .ToQuery("format", "24+hourly")
                .ToQuery("numDays", "7");

            return new List<HttpQueryRegistration> { weather };
        }

        protected override IList<HttpCallDescription> ConfigureOperations()
        {
            // For each entity that supports target operations, configure an operation.
            return new List<HttpCallDescription> { };
        }
    }
}
