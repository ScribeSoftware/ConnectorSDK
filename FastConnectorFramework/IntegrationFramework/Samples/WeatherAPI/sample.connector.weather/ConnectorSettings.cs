﻿namespace Sample.Connector.Weather
{
    /// <summary>
    /// Used for Discovery. These constants are used to fill in some of the values in the
    /// ScribeConnectorAttrribute declaration on the class that implements IConnector.
    /// Connector settings that are often or must different from other connectors.
    /// </summary>
    public class ConnectorSettings
    {
        public const string ConnectorTypeId = "e5e49643-7bb7-4a18-9a4b-0a53531279c2";
        public const string ConnectorVersion = "1.0";
        public const string Description = "Sample Connector that gets weather information.";
        public const string Name = "SampleWeather";
        public const bool SupportsCloud = true;
        public const string CompanyName = "Sample";
        public const string AppName = "Weather";
        public const string Copyright = "Copyright © 2015 Sample All rights reserved.";
    }
}
