﻿namespace YourCompany.Connector.ScribeOrganizations
{
    using Simple.Connector.Framework;
    using Simple.Connector.Framework.Http;

    [SimpleConnector(
        ConnectorSettings.ConnectorTypeId,
        ConnectorSettings.Name,
        ConnectorSettings.Description,
        typeof(ScribeOrganizationsConnector),
        ConnectorSettings.SupportsCloud,
        ConnectorSettings.ConnectorVersion)]
    public class ScribeOrganizationsConnector : HttpConnectorBase<ScribeOrganizationsConnectionInfo>
    {
        public ScribeOrganizationsConnector()
            : base(ConnectorSettings.ConnectorTypeId, "YourCompany", "http://www.scribesoft.com", "f3b42156-632e-4948-abbc-b158bfdf52b9")
        {
        }
    }
}
