﻿namespace YourCompany.Connector.ScribeOrganizations
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    using Simple.Connector.Framework.Http;

    using Models;

    public class ScribeOrganizationsConnectionInfo : JsonHttpConnectionInfoBase<ScribeOrganizationsConnectionInfo>
    {
        public string BaseUrl { get; set; }

        public string OrgId { get; set; }

        public string UserName { get; set; }

        [PasswordPropertyText(true)]
        public string Password { get; set; }

        protected override Func<ScribeOrganizationsConnectionInfo, IConnectionConfiguration> ConnectionConfiguration()
        {
            return this.Connection.ConfigureWithBasicAuth((form) => form.BaseUrl + "/v1/orgs/" + form.OrgId + "/", this.UserName, this.Password)
                .ConnectionInfoToBaseUrl(info => info.BaseUrl) // Set the BaseUrl for subsequent calls
                .End(); // Finish the configuration
        }
        protected override IList<HttpQueryRegistration> ConfigureQueries()
        {
            var organization = this.Queries.EnumerateResponseAs<Organization, Organization[]>("/v1/orgs", r => r);

            return new List<HttpQueryRegistration> { organization };
        }

        protected override IList<HttpCallDescription> ConfigureOperations()
        {
            // For each entity that supports target operations, configure an operation.
            // Updates
            var updateOrganization = this.Operations.Update<Organization>("/v1/orgs/" + this.OrgId);

            return new List<HttpCallDescription> { updateOrganization };
        }
    }
}
