/*    SampleRSSource.sql                                                                      */
/*                                                                                            */
/*    This script creates a sample RS source database structure.                              */


/* Create ScribeSampleRSSource database                                                       */
use master
go

if not exists(select * from dbo.sysdatabases where name = 'ScribeSampleRSSource')
    create database "ScribeSampleRSSource"
go

use "ScribeSampleRSSource"
go


/* Drop Tables - If They Exist                                                                */
if exists (select * from dbo.sysobjects where id = object_id('dbo.SalesOrderDetails')
    and OBJECTPROPERTY(id, 'IsUserTable') = 1)
    drop table dbo.SalesOrderDetails
go
if exists (select * from dbo.sysobjects where id = object_id('dbo.SalesOrders')
    and OBJECTPROPERTY(id, 'IsUserTable') = 1)
    drop table dbo.SalesOrders
go
if exists (select * from dbo.sysobjects where id = object_id('dbo.Addresses')
    and OBJECTPROPERTY(id, 'IsUserTable') = 1)
    drop table dbo.Addresses
go
if exists (select * from dbo.sysobjects where id = object_id('dbo.Customers')
    and OBJECTPROPERTY(id, 'IsUserTable') = 1)
    drop table dbo.Customers
go
if exists (select * from dbo.sysobjects where id = object_id('dbo.ProductPriceLists')
    and OBJECTPROPERTY(id, 'IsUserTable') = 1)
    drop table dbo.ProductPriceLists
go
if exists (select * from dbo.sysobjects where id = object_id('dbo.Products')
    and OBJECTPROPERTY(id, 'IsUserTable') = 1)
    drop table dbo.Products
go
if exists (select * from dbo.sysobjects where id = object_id('dbo.PickLists')
    and OBJECTPROPERTY(id, 'IsUserTable') = 1)
    drop table dbo.PickLists
go
if exists (select * from dbo.sysobjects where id = object_id('dbo.GeneralLedger')
    and OBJECTPROPERTY(id, 'IsUserTable') = 1)
    drop table dbo.GeneralLedger
go

/* Drop stored procedures - If they Exist */
if exists (select * from dbo.sysobjects where id = object_id('dbo.spRecalcOrder')
    and OBJECTPROPERTY(id, 'IsProcedure') = 1)
    drop proc dbo.spRecalcOrder
go


/* Create new table "PickLists".                                                              */
/* "PickLists" : Table of PickLists - used for:                                               */
/* 	Order Types                                                                               */
/* 	Order Status                                                                              */
/* 	Units of Measure & Schedules                                                              */
/* 	Price Lists                                                                               */
/* 	Shipping Methods                                                                          */
/* 	Address Types                                                                             */
/* 	Payment Terms                                                                             */
/* 	Region                                                                                    */
/* 	State                                                                                     */
create table dbo."PickLists" ( 
	"RecordId" uniqueidentifier rowguidcol unique default newid() not null,
	"PickListName" nvarchar(20) not null,
	"Code" nvarchar(20) not null,
	"Description" nvarchar(50) null) ON 'PRIMARY'  
go

alter table "PickLists"
	add constraint "PickLists_PK" primary key clustered ("PickListName", "Code")   
go

grant select, insert, update, delete on dbo.PickLists to public
go


/* Create new table "Addresses".                                                              */
/* "Addresses" : Table of Addresses                                                           */
create table dbo."Addresses" ( 
	"RecordId" uniqueidentifier rowguidcol unique default newid() not null,
	"CustomerNumber" nvarchar(15) not null,
	"LocationName" nvarchar(15) not null,
	"AddressType" nvarchar(15) not null,
	"ContactName" nvarchar(50) null,
	"ContactTitle" nvarchar(50) null,
	"AddressLine1" nvarchar(50) not null,
	"AddressLine2" nvarchar(50) null,
	"City" nvarchar(25) not null,
	"State" nchar(2) null,
	"PostalCode" nvarchar(10) not null,
	"Country" nvarchar(15) null,
	"Phone" nvarchar(24) null,
	"Fax" nvarchar(24) null,
	"ShippingMethod" nvarchar(10) null,
	"TaxSchedule" nvarchar(15) null,
	"CreatedOn" datetime default getdate() not null,
	"CreatedBy" nvarchar(32) default SYSTEM_USER not null,
	"ModifiedOn" datetime default getdate() not null,
	"ModifiedBy" nvarchar(32) default SYSTEM_USER not null) ON 'PRIMARY'  
go

alter table "Addresses"
	add constraint "Addresses_PK" primary key clustered ("CustomerNumber", "LocationName")   
go

grant select, insert, update, delete on dbo.Addresses to public
go

EXEC sys.sp_addextendedproperty @name=N'Description', @value=N'This is the table that contains customer addresses' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Addresses'
GO

/* Create new table "ProductPriceLists".                                                      */
/* "ProductPriceLists" : Table of ProductPriceLists                                           */
create table dbo."ProductPriceLists" ( 
	"RecordId" uniqueidentifier rowguidcol unique default newid() not null,
	"ProductNumber" nvarchar(15) not null,
	"PriceList" nvarchar(15) not null,
	"UnitOfMeasure" nvarchar(12) not null,
	"UnitPrice" money not null,
	"BaseUoMQuantity" int null,
	"CreatedOn" datetime default getdate() not null,
	"CreatedBy" nvarchar(32) default SYSTEM_USER not null,	
	"ModifiedOn" datetime default getdate() not null,
	"ModifiedBy" nvarchar(32) default SYSTEM_USER not null) ON 'PRIMARY'  
go

alter table "ProductPriceLists"
	add constraint "ProductPriceLists_PK" primary key clustered ("ProductNumber", "PriceList", "UnitOfMeasure")   
go

grant select, insert, update, delete on dbo.ProductPriceLists to public
go


/* Create new table "Products".                                                               */
/* "Products" : Table of Products                                                             */
create table dbo."Products" ( 
	"RecordId" uniqueidentifier rowguidcol unique default newid() not null,
	"ProductNumber" nvarchar(15) not null,
	"ProductName" nvarchar(50) not null,
	"Type" nvarchar(12) not null,
	"UoMSchedule" nvarchar(15) null,
	"ListPrice" money null,
	"Cost" money null,
	"StandardCost" money null,
	"QuantityInStock" int null,
	"QuantityOnOrder" int null,
	"Discontinued" smallint default 0 not null constraint "ProductsDiscontinued_Chk" check ("Discontinued" between 0 and 1) ,
	"CreatedOn" datetime default getdate() not null,
	"CreatedBy" nvarchar(32) default SYSTEM_USER not null,
	"ModifiedOn" datetime default getdate() not null,
	"ModifiedBy" nvarchar(32) default SYSTEM_USER not null) ON 'PRIMARY'  
go

alter table "Products"
	add constraint "PK_Products" primary key clustered ("ProductNumber")   
go

grant select, insert, update, delete on dbo.Products to public
go


/* Create new table "SalesOrders".                                                            */
/* "SalesOrders" : Table of SalesOrders                                                       */
create table dbo."SalesOrders" ( 
	"RecordId" uniqueidentifier rowguidcol unique default newid() not null,
	"OrderNumber" nvarchar(15) not null,
	"CustomerNumber" nvarchar(15) not null,
	"BillToAddrName" nvarchar(15) not null,
	"Type" nvarchar(8) not null constraint "SalesOrdersType_Chk" check ("Type" in ('Order','Invoice','Quote')) ,
	"Status" nvarchar(6) not null constraint "SalesOrdersStatus_Chk" check ("Status" in ('Draft','Open','Closed', 'Void', 'Posted')) ,
	"OrderDate" datetime not null,
	"SalespersonID" nvarchar(10) null,
	"ShippedDate" datetime null,
	"ShippingMethod" nvarchar(10) not null,
	"PriceList" nvarchar(15) not null,
	"PaymentTerms" nvarchar(10) not null,
	"Freight" money default 0 not null,
	"Discount" money default 0 not null,
	"Tax" money default 0 not null,
	"TotalAmount" money default 0 not null,
	"RequestedShipDate" datetime null,
	"ShipContact" nvarchar(50) null,
	"ShipAddressLine1" nvarchar(50) not null,
	"ShipAddressLine2" nvarchar(50) null,
	"ShipCity" nvarchar(25) not null,
	"ShipState" nchar(2) null,
	"ShipPostalCode" nvarchar(10) not null,
	"ShipCountry" nvarchar(15) null,
	"ShipPhone" nvarchar(24) null,
	"TaxSchedule" nvarchar(15) null,
	"CreatedOn" datetime default getdate() not null,
	"CreatedBy" nvarchar(32) default SYSTEM_USER not null,
	"ModifiedOn" datetime default getdate() not null,
	"ModifiedBy" nvarchar(32) default SYSTEM_USER not null,
	"OriginalOrderNumber" nvarchar(15) null) ON 'PRIMARY'  
go

alter table "SalesOrders"
	add constraint "PK_Orders" primary key clustered ("OrderNumber")   
go

grant select, insert, update, delete on dbo.SalesOrders to public
go


/* Create new table "SalesOrderDetails".                                                      */
/* "SalesOrderDetails" : Table of SalesOrderDetails                                           */
create table dbo."SalesOrderDetails" ( 
	"RecordId" uniqueidentifier rowguidcol unique default newid() not null,
	"OrderNumber" nvarchar(15) not null,
	"LineNumber" int not null,
	"InventoryItem" smallint default 1 not null constraint "SalesOrderDetailsInventoryItem_Chk" check ("InventoryItem" between 0 and 1) ,
	"ProductNumber" nvarchar(15) null,
	"Description" nvarchar(60) null,
	"UnitOfMeasure" nvarchar(12) not null,
	"Quantity" int not null,
	"UnitPrice" money not null,
	"ItemDiscount" money default 0 not null,
	"ItemTax" money default 0 not null,
	"ExtendedPrice" money not null,
	"RequestedShipDate" datetime null,
	"QuantityShipped" int null,
	"QuantityCancelled" int null,
	"CreatedOn" datetime default getdate() not null,
	"CreatedBy" nvarchar(32) default SYSTEM_USER not null,	
	"ModifiedOn" datetime default getdate() not null,
	"ModifiedBy" nvarchar(32) default SYSTEM_USER not null) ON 'PRIMARY'  
go

alter table "SalesOrderDetails"
	add constraint "PK_Order_Details" primary key clustered ("OrderNumber", "LineNumber")   
go

grant select, insert, update, delete on dbo.SalesOrderDetails to public
go


/* Create new table "Customers".                                                              */
/* "Customers" : Table of Customers                                                           */
create table dbo."Customers" ( 
	"RecordId" uniqueidentifier rowguidcol unique default newid() not null,
	"CustomerNumber" nvarchar(15) not null,
	"CompanyName" nvarchar(50) not null,
	"Region" nvarchar(15) null,
	"ContactName" nvarchar(50) null,
	"ContactTitle" nvarchar(50) null,
	"PrimaryAddrName" nvarchar(15) null,
	"Phone" nvarchar(24) null,
	"Fax" nvarchar(24) null,
	"Email" nvarchar(50) null,
	"WebsiteURL" nvarchar(100) null,
	"PriceList" nvarchar(15) null,
	"CreditOnHold" smallint null constraint "CustomersCreditOnHold_Chk" check ("CreditOnHold" between 0 and 1),
	"CreditAmount" money null,
	"PaymentTerms" nvarchar(10) null,
	"Active" smallint default 1 not null constraint "CustomersActive_Chk" check ("Active" between 0 and 1),
	"XRefID" nvarchar(50) null,
	"IntegratedFlag" smallint null,
	"CreatedOn" datetime default getdate() not null,
	"CreatedBy" nvarchar(32) default SYSTEM_USER not null,
	"ModifiedOn" datetime default getdate() not null,
	"ModifiedBy" nvarchar(32) default SYSTEM_USER not null) ON 'PRIMARY'  
go

alter table "Customers"
	add constraint "PK_Customers" primary key clustered ("CustomerNumber")   
go

grant select, insert, update, delete on dbo.Customers to public
go


/* Create new table "GeneralLedger".                                                          */
/* "GeneralLedger" : Table of General Ledger Transactions                                     */
create table dbo."GeneralLedger" ( 
	"RecordId" uniqueidentifier rowguidcol unique default newid() not null,
	"BatchID" nvarchar(15) not null,
	"JournalEntry" nvarchar(15) not null,
	"Reference" nvarchar(15) not null,
	"TransactionDate" nvarchar(20) null,
	"Description" nvarchar (20) null,
	"DebitAmount" nvarchar(15) not null,
	"DebitAccountString" nvarchar(50) not null,
	"Description2" nvarchar(20) null,
	"CreditAmount" nvarchar(15) not null,
	"CreditAccountString" nvarchar(50) not null,
	"UserID" nvarchar(10) null,
	"CreatedOn" datetime default getdate() not null,
	"CreatedBy" nvarchar(32) default SYSTEM_USER not null,
	"ModifiedOn" datetime default getdate() not null,
	"ModifiedBy" nvarchar(32) default SYSTEM_USER not null) ON 'PRIMARY'  
go

alter table "GeneralLedger"
	add constraint "PK_GeneralLedger" primary key clustered ("BatchID", "JournalEntry")   
go

grant select, insert, update, delete on dbo.GeneralLedger to public
go


EXEC sys.sp_addextendedproperty @name=N'Description', @value=N'This table contains all customer billing information' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customers'
GO

/* Add the remaining keys, constraints and indexes for the table "Addresses".                 */
create index "PostalCode" on "Addresses" (
	"PostalCode")  
go

create index "ModifiedOn" on "Addresses" (
	"ModifiedOn")  
go

/* Add the remaining keys, constraints and indexes for the table "ProductPriceLists".         */
create index "ModifiedOn" on "ProductPriceLists" (
	"ModifiedOn")  
go

/* Add the remaining keys, constraints and indexes for the table "Products".                  */
create index "ProductName" on "Products" (
	"ProductName") ON 'PRIMARY'  
go

create index "ModifiedOn" on "Products" (
	"ModifiedOn")  
go

/* Add the remaining keys, constraints and indexes for the table "SalesOrders".               */
create index "CustomerNumber" on "SalesOrders" (
	"CustomerNumber",
	"Type",
	"OrderDate") ON 'PRIMARY'  
go

create index "OrderDate" on "SalesOrders" (
	"Type",
	"OrderDate") ON 'PRIMARY'  
go

create index "ModifiedOn" on "SalesOrders" (
	"ModifiedOn")  
go

/* Add the remaining keys, constraints and indexes for the table "SalesOrderDetails".         */
create index "ModifiedOn" on "SalesOrderDetails" (
	"ModifiedOn")  
go

/* Add the remaining keys, constraints and indexes for the table "Customers".                 */
create index "CompanyName" on "Customers" (
	"CompanyName") ON 'PRIMARY'  
go

create index "ModifiedOn" on "Customers" (
	"ModifiedOn")  
go

/* Add the remaining keys, constraints and indexes for the table "GeneralLedger".             */
create index "ModifiedOn" on "GeneralLedger" (
	"ModifiedOn")  
go

/* Add foreign key constraints to table "Addresses".                                          */
alter table "Addresses"
	add constraint "Customers_Addresses_FK1" foreign key (
		"CustomerNumber")
	 references "Customers" (
		"CustomerNumber") on update no action on delete cascade  
go

/* Add foreign key constraints to table "ProductPriceLists".                                  */
alter table "ProductPriceLists"
	add constraint "Products_ProductPriceLists_FK1" foreign key (
		"ProductNumber")
	 references "Products" (
		"ProductNumber") on update no action on delete cascade  
go

/* Add foreign key constraints to table "SalesOrders".                                        */
alter table "SalesOrders"
	add constraint "FK_Orders_Customers" foreign key (
		"CustomerNumber")
	 references "Customers" (
		"CustomerNumber") on update no action on delete no action  
go

alter table "SalesOrders"
	add constraint "Addresses_SalesOrders_FK1" foreign key (
		"CustomerNumber",
		"BillToAddrName")
	 references "Addresses" (
		"CustomerNumber",
		"LocationName") on update no action on delete no action  
go

/* Add foreign key constraints to table "SalesOrderDetails".                                  */
alter table "SalesOrderDetails"
	add constraint "FK_Order_Details_Orders" foreign key (
		"OrderNumber")
	 references "SalesOrders" (
		"OrderNumber") on update no action on delete cascade  
go

/* Create/Recreate user defined stored procedures that use the newly create and changed tables.         */

/* Create spRecalcOrder stored procedure */
create proc [dbo].[spRecalcOrder] @orderNumber nvarchar(15)
as
begin
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @detailTotal money
	DECLARE @freight money
	DECLARE @tax money
	DECLARE @discount money
	DECLARE @total money

	/* fetch sum of SalesOrderDetails ExtendedPrices */
	select @detailTotal = sum (ExtendedPrice) from SalesOrderDetails
	where OrderNumber = @orderNumber

	if @detailTotal is NULL 
		set @detailTotal = 0

	/* fetch other key fields from SalesOrders */
	select @freight = Freight, @tax = Tax, @discount = Discount from SalesOrders
	where OrderNumber = @orderNumber

	/* Re-calc & update SalesOrders.TotalAmount */
	select @total = (@detailTotal + @freight + @tax ) - @discount
	update SalesOrders set TotalAmount = @total 
	where OrderNumber = @orderNumber

end;
go

/*** Create View: Customer_Address ***
*/

if exists (select * from dbo.sysobjects where id = object_id('dbo.Customer_Address')
    and OBJECTPROPERTY(id, 'IsView') = 1)
    drop view dbo.Customer_Address
go

create view dbo.Customer_Address as
    select c.*,
        a.LocationName, a.AddressType, a.AddressLine1, a.AddressLine2, a.City, a.State,
        a.PostalCode, a.Country
    from dbo.Customers c
    join dbo.Addresses a on c.CustomerNumber = a.CustomerNumber and c.PrimaryAddrName = a.LocationName
go

grant select on dbo.Customer_Address to public
go
