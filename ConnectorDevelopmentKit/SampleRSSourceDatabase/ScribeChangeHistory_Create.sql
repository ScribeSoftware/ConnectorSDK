USE [ScribeSampleRSSource]

CREATE TABLE [dbo].[ScribeChangeHistory](
	[RecordId] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[TableName] [nvarchar](100) NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[Operation] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](32) NOT NULL,
UNIQUE NONCLUSTERED 
(
	[RecordId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[ScribeChangeHistory] ADD  DEFAULT (newid()) FOR [RecordId]

