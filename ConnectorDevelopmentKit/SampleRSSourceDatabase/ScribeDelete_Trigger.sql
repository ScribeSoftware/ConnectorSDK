create trigger [dbo].[{0}_Deleted_TRG]
on [dbo].[{0}]
for delete as
begin
	declare @id uniqueidentifier

	/* create cursor and walk all affected records, updating */
	/* ScribeChangeHistory for each                          */
    declare trgtblCursor cursor local for
        select RecordId from DELETED

    open trgtblCursor
    fetch next from trgtblCursor into @id
    while @@FETCH_STATUS = 0
    begin

		/* add record to ScribeChangeHistory */
		insert into ScribeChangeHistory (TableName,Id,Operation,ModifiedOn,ModifiedBy) 
			values ('{0}',@id,'DELETE',getdate(),SYSTEM_USER )

        fetch next from trgtblCursor into @id
    end
	
	/* cleanup cursor */
    close trgtblCursor
    deallocate trgtblCursor
    
end;


