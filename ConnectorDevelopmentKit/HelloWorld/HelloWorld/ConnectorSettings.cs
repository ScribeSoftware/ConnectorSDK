﻿namespace HelloWorld
{
    public class ConnectorSettings
    {
        public const string ConnectorTypeId = "{78E93E94-237F-4AF9-A8C7-A5AFB9BD1B36}";
        public const string ConnectorVersion = "1.0";
        public const string Description = "Hello World connector.";
        public const string Name = "HelloWorld";
        public const bool SupportsCloud = false;
    }
}